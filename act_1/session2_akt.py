"""

Autor: Artur Kravchuk Tarasyuk
Este programa es una calculadora simple que te permite hacer operaciones con varios numeros.

"""

print(__doc__)

# Muestra el Menu por pantalla.

print("__Menu__")
print("1 - Sumas")
print("2 - Restas")
print("3 - Multiplicar")
print("4 - Dividir")
print("5 - Todas")
print("6 - Salir")


# Selecciona una opción
sel = int(input("selección: "))


# Función para resolver una operación de suma.
def sumar(suma_ints):
    res = 0
    for x in suma_ints:
        res += x
    print(res)
    return res

# Función para resolver una operación de resta.
def restar(resta_ints):
    res = 0
    for x in resta_ints:
        if(x==resta_ints[0]):
            resta1 = x
        else:
            resta1 -= x
        res = resta1
    print(res)
    return res


# Función para resolver una operación de multiplicación.
def multiplicar(multiplicacion_ints):
    res = 0
    for x in multiplicacion_ints:
        if(x==multiplicacion_ints[0]):
            multiplicacion1 = x
        else:
            multiplicacion1 *= x
        res = multiplicacion1
    print(res)
    return res


# Función para resolver una operación de division.
def dividir(division_ints):
    res = 0 
    for x in division_ints:
        if(x==division_ints[0]):
            division1 = x
        else:
            division1 /= x
        res = division1
    print(res)
    return res

# Al seleccionar el 1 del menu te pide que des la operación de la suma, ejemplos: 
# 1+4
# 2+2
# 345+13+4
# 5+1+23+4+51+34+1
# Luego lo separa en una array de ints y llama la función sumar(suma_ints)

if sel==1:
    suma = input("suma: ")
    suma_separada = suma.split("+")
    suma_ints = [eval(i) for i in suma_separada]
    sumar(suma_ints)



# Al seleccionar el 2 del menu te pide que des la operación de la resta, ejemplos: 
# 5-4
# 3-2
# 345-154-1
# 553-52-14-11-3
# Luego lo separa en una array de ints y llama la función restar(resta_ints)

elif sel==2:
    resta = input("resta: ")
    resta_separada = resta.split("-")
    resta_ints = [eval(i) for i in resta_separada]
    restar(resta_ints)


# Al seleccionar el 3 del menu te pide que des la operación de la multiplicación, ejemplos: 
# 5*5
# 7*3
# 34*13*2
# 51*23*4*51*34*1
# Luego lo separa en una array de ints y llama la función multiplicar(multiplicacion_ints)


elif sel==3:
    multiplicacion = input("multiplicacion: ")
    multiplicacion_separada = multiplicacion.split("*")
    multiplicacion_ints = [eval(i) for i in multiplicacion_separada]
    multiplicar(multiplicacion_ints)

# Al seleccionar el 4 del menu te pide que des la operación de la division, ejemplos: 
# 1/4
# 2/2
# 345/13/4
# 5/1/23/4/51/41
# Luego lo separa en una array de ints y llama la función dividir(division_ints)
# Si divides por 0, da fallo.


elif sel==4:
    division = input("division: ")
    division_separada = division.split("/")
    division_ints = [eval(i) for i in division_separada]
    try:
        dividir(division_ints)
    except ZeroDivisionError:
        print("No puedes dividir por 0...")
else:
    exit()
